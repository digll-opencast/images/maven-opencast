## Docker Opencast - Maven Base-Image ##

This repository contains build instructions for a container image based on the official
maven image, but filled with a pre-cached maven repository.

### Building the image ###

Building the image can be achieved with the following commands:
```bash
git clone https://gitlab.digll-hessen.de/OC-Cluster/images/Maven-Opencast.git
cd Maven-Opencast

# Build using the buildah tool
sudo buildah build-using-dockerfile --tag maven-opencast:master -f Dockerfile .
```

(*Replace `buildah build-using-dockerfile` with `docker build` when using the docker deamon*)

### Publishing the image ###

The image should to pushed to your local (*gitlab*) image registry for use in your container infrastructure.

Manually pushing image using *buildah* tool (after having been build):
```bash
sudo buildah push maven-opencast:master docker://registry.gitlab.com/digll-opencast/images/Maven-Opencast:master
```
(*Replace `buildah` with `docker` when using the docker deamon*)

This repository contains Gitlab CI-Runner job configurations for building and pushing build-artifacts
to your local container registry, assuming it has support for this. Images will be tagged according to branch-name.

### Using the image ###

This images does not have any entry-point or command and should only be used as base-image
when working with the Opencast maven project.

### Configuration ###

This image (re-)uses the following environment variables during build to find the location of the local Opencast maven repository:

* `OPENCAST_HOME` - Location of Opencast build-user home directory
* `OPENCAST_UID` - Unique identifier of build-user
* `OPENCAST_GID`- Unique identifier of build-user main group

### Updating assets ###

The pre-cached maven repository can be updated by pulling the latest Opencast repository from <https://github.com/opencast/opencast>
and running `mvn dependency:resolve` inside. The local maven repository should then be available under `${HOME}/.m2/repository`.
