# Use Maven (Java 8) as base for building
FROM maven:3.6-jdk-8-slim AS build

# Environment variables also used by opencast
ENV OPENCAST_HOME="/opencast" \
    OPENCAST_UID="800" \
    OPENCAST_GID="800"

# Copy nexus.opencast.org mirror configuration into image
COPY "assets/settings.xml" "/usr/share/maven/conf/settings.xml"

# Copy pre-filled maven repository into image
COPY --chown="${OPENCAST_UID}:${OPENCAST_GID}" "assets/repository" "${OPENCAST_HOME}/.m2/repository"
